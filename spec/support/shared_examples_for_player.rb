require 'spec_helper'

RSpec.shared_examples TicTacToe::Player do
  let(:marker) { double }
  let(:player) { described_class.new(marker) }

  it { expect(player).to respond_to(:play!) }

  describe "#initialize" do
    it "receives the player marker" do
      expect(player.marker).to eq(marker)
    end
  end
end
