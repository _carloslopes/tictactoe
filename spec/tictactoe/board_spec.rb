require 'spec_helper'

RSpec.describe TicTacToe::Board do
  let(:board) { described_class.new }

  describe "#print" do
    it "prints the board" do
      expect {
        board.print
      }.to output(<<~STR).to_stdout
       0 | 1 | 2
      ===x===x===
       3 | 4 | 5
      ===x===x===
       6 | 7 | 8
      STR
    end
  end

  describe "#has_square?" do
    it { expect(board).not_to have_square(9 + rand(100)) }
    it { expect(board).not_to have_square(-1 - rand(100)) }

    ("0".."8").each do |i|
      it { expect(board).to have_square(i) }
    end
  end

  describe "#square_filled?" do
    let(:square) { rand(8) }

    it "returns false when the square isn't filled" do
      expect(board.square_filled?(square)).to be(false)
    end

    it "returns true when the square is already filled" do
      board[square] = "X"

      expect(board.square_filled?(square)).to be(true)
    end
  end

  describe "#[]=" do
    let(:square) { rand(8) }

    it "fills the informed square with the value provided" do
      expect(board.square_filled?(square)).to be(false)

      board[square] = "O"

      expect(board.square_filled?(square)).to be(true)
    end
  end

  describe "#[]" do
    let(:square) { rand(8) }

    it "returns the value on the square" do
      board[square] = "O"

      expect(board[square]).to eq("O")
    end
  end

  describe "#has_winner?" do
    let(:mark) { ["X", "O"].sample }

    it "returns false when the board is fresh" do
      expect(board).not_to have_winner
    end

    it "returns false when a row isn't completly filled by a unique mark" do
      board[0] = "X"
      board[1] = nil
      board[2] = "O"

      expect(board).not_to have_winner
    end

    it "returns false when a column isn't completly filled by a unique mark" do
      board[0] = "O"
      board[3] = "O"
      board[6] = "X"

      expect(board).not_to have_winner
    end

    # Test when each row of the board is filled
    [[0, 1, 2], [3, 4, 5], [6, 7, 8]].each_with_index do |arr, i|
      it "returns true when row #{i + 1} is completly filled by a unique mark" do
        arr.each { |square| board[square] = mark }

        expect(board).to have_winner
      end
    end

    # Test when each column of the board is filled
    [[0, 3, 6], [1, 4, 7], [2, 5, 8]].each_with_index do |arr, i|
      it "returns true when column #{i + 1} is completly filled by a unique mark" do
        arr.each { |square| board[square] = mark }

        expect(board).to have_winner
      end
    end

    # Test when each transversal of the board is filled
    [[0, 4, 8], [2, 4, 6]].each_with_index do |arr, i|
      it "returns true when transversal #{i + 1} is completly filled by a unique mark" do
        arr.each { |square| board[square] = mark }

        expect(board).to have_winner
      end
    end
  end

  describe "#game_is_over?" do
    before(:each) do
      allow(board).to receive(:has_winner?).and_return(have_winner)
    end

    context "when the board has a winner" do
      let(:have_winner) { true }

      it "returns true" do
        expect(board.game_is_over?).to be true
      end
    end

    context "when the board doesn't have a winner" do
      let(:have_winner) { false }

      it "returns true when all squares are filled" do
        9.times { |i| board[i] = "X" }

        expect(board.game_is_over?).to be true
      end

      it "returns false when there are squares not filled" do
        6.times { |i| board[i] = "X" }

        expect(board.game_is_over?).to be false
      end
    end
  end

  describe "#tie?" do
    before(:each) do
      allow(board).to receive(:has_winner?).and_return(have_winner)
    end

    context "when the board has a winner" do
      let(:have_winner) { true }

      it "returns false" do
        expect(board.tie?).to be false
      end
    end

    context "when the board doesn't have a winner" do
      let(:have_winner) { false }

      it "returns true when all squares are filled" do
        9.times { |i| board[i] = "X" }

        expect(board.tie?).to be true
      end

      it "returns false when there are squares not filled" do
        6.times { |i| board[i] = "X" }

        expect(board.tie?).to be false
      end
    end
  end

  describe "#available_squares" do
    it "returns an array with the available squares ids" do
      squares = (0..8).to_a
      filled_squares = squares.sample(rand(8))

      filled_squares.each do |square|
        board[square] = ["X", "O"].sample
      end

      expect(board.available_squares).to eq(squares - filled_squares)
    end
  end

  describe "#game_would_be_over?" do
    let(:square) { rand(8) }
    let(:game_would_be_over?) { board.game_would_be_over?(square, double) }

    context "when the movement checked would cause a game over" do
      before(:each) do
        allow(board).to receive(:game_is_over?).and_return(true)
      end

      it "returns true" do
        expect(game_would_be_over?).to be true
      end

      it "doesn't fill the square" do
        game_would_be_over?

        expect(board[square]).to be_nil
      end
    end

    context "when the movement being checked would be indifferent" do
      before(:each) do
        allow(board).to receive(:game_is_over?).and_return(false)
      end

      it "returns false" do
        expect(game_would_be_over?).to be false
      end

      it "doesn't fill the square" do
        game_would_be_over?

        expect(board[square]).to be_nil
      end
    end
  end

  describe "#marker_won?" do
    let(:marker) { double }

    context "when the marker has a win match" do
      it "returns true" do
        win_match = described_class::WIN_MATCHES.sample

        win_match.each { |i| board[i] = marker }

        expect(board.marker_won?(marker)).to be(true)
      end
    end

    context "when the marker doesn't have a win match" do
      it "returns false" do
        expect(board.marker_won?(marker)).to be(false)
      end
    end
  end

end
