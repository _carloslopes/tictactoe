require 'spec_helper'

RSpec.describe TicTacToe::Game do

  describe ".run" do
    it "initiates a new game, setup and start it" do
      game = double

      allow(described_class).to receive(:new).and_return(game)

      expect(game).to receive(:setup)
      expect(game).to receive(:start)
      expect(game).to receive(:summary)

      described_class.run
    end
  end

end
