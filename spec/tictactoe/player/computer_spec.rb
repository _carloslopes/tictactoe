require 'spec_helper'

RSpec.describe TicTacToe::Player::Computer do
  it_behaves_like TicTacToe::Player

  let(:marker)  { described_class::MARKERS.sample }
  let(:computer) { described_class.new(marker, mode: mode) }

  describe "#play!" do
    let(:board) { double }
    subject { computer.play!(board) }

    before(:each) do
      allow(board).to receive(:available_squares).and_return([0, 8])
    end

    context "when the computer is on hard mode" do
      let(:mode) { :hard }

      context "and the middle square isn't filled" do
        it "fills the middle square" do
          allow(board).to receive(:square_filled?).with(4).and_return(false)

          expect(board).to receive(:[]=).with(4, marker)

          subject
        end
      end

      context "and the middle square is already filled" do
        before(:each) do
          allow(board).to receive(:square_filled?).with(4).and_return(true)
          allow(board).to receive(:game_would_be_over?)
        end

        context "and there's one movement missing to win" do
          it "fills the missing square to win" do
            allow(board).to receive(:game_would_be_over?).with(8, marker).and_return(true)

            expect(board).to receive(:[]=).with(8, marker)

            subject
          end
        end

        context "and there's one movement missing to loose" do
          let(:marker) { "O" }

          it "fills the square to avoid loose" do
            allow(board).to receive(:game_would_be_over?).with(8, "X").and_return(true)

            expect(board).to receive(:[]=).with(8, marker)

            subject
          end
        end

        context "and there's no movement to win or loose" do
          it "fills a random square" do
            square = double

            allow(board.available_squares).to receive(:sample).and_return(square)

            expect(board).to receive(:[]=).with(square, marker)

            subject
          end
        end
      end
    end

    context "when the computer is on medium mode" do
      let(:mode) { :medium }

      context "and the middle square isn't filled" do
        it "fills the middle square" do
          allow(board).to receive(:square_filled?).with(4).and_return(false)

          expect(board).to receive(:[]=).with(4, marker)

          subject
        end
      end

      context "and the middle square is already filled" do
        before(:each) do
          allow(board).to receive(:square_filled?).with(4).and_return(true)
          allow(board).to receive(:game_would_be_over?)
        end

        context "and there's one movement missing to win" do
          it "fills the missing square to win" do
            allow(board).to receive(:game_would_be_over?).with(8, marker).and_return(true)

            expect(board).to receive(:[]=).with(8, marker)

            subject
          end
        end

        context "and there's one movement missing to loose" do
          let(:marker) { "O" }

          it "fills a random square" do
            square = double

            allow(board).to receive(:game_would_be_over?).with(8, "X").and_return(true)
            allow(board.available_squares).to receive(:sample).and_return(square)

            expect(board).to receive(:[]=).with(square, marker)

            subject
          end
        end

        context "and there's no movement to win" do
          it "fills a random square" do
            square = double

            allow(board.available_squares).to receive(:sample).and_return(square)

            expect(board).to receive(:[]=).with(square, marker)

            subject
          end
        end
      end
    end

    context "when the computer is on easy mode" do
      let(:mode) { :easy }

      context "and the middle square isn't filled" do
        it "fills the middle square" do
          allow(board).to receive(:square_filled?).with(4).and_return(false)

          expect(board).to receive(:[]=).with(4, marker)

          subject
        end
      end

      context "and the middle square is already filled" do
        before(:each) do
          allow(board).to receive(:square_filled?).with(4).and_return(true)
          allow(board).to receive(:game_would_be_over?)
        end

        context "and there's one movement missing to win" do
          it "fills a random square" do
            square = double

            allow(board).to receive(:game_would_be_over?).with(8, marker).and_return(true)
            allow(board.available_squares).to receive(:sample).and_return(square)

            expect(board).to receive(:[]=).with(square, marker)

            subject
          end
        end

        context "and there's one movement missing to loose" do
          let(:marker) { "O" }

          it "fills a random square" do
            square = double

            allow(board).to receive(:game_would_be_over?).with(8, "X").and_return(true)
            allow(board.available_squares).to receive(:sample).and_return(square)

            expect(board).to receive(:[]=).with(square, marker)

            subject
          end
        end

        context "and there's no movement to win" do
          it "fills a random square" do
            square = double

            allow(board.available_squares).to receive(:sample).and_return(square)

            expect(board).to receive(:[]=).with(square, marker)

            subject
          end
        end
      end
    end

  end

end
