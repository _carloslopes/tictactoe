require 'spec_helper'

RSpec.describe TicTacToe::Player::Human do
  it_behaves_like TicTacToe::Player

  let(:marker)  { described_class::MARKERS.sample }
  let(:human) { described_class.new(marker) }

  describe "#play!" do
    let(:board) { double.as_null_object }
    subject { human.play!(board) }

    before(:each) do
      allow(human).to receive(:gets).and_return(*user_input)
      allow(human).to receive(:print)
      allow(human).to receive(:puts)
      allow(human).to receive(:exit)
    end

    shared_examples :before_user_input_messages do
      it "prints the board" do
        expect(board).to receive(:print)

        subject
      end

      it "asks user for his input" do
        expect(human).to receive(:print).with("It's your turn '#{marker}'! Enter [0-8] to play or [q] to quit: ")

        subject
      end
    end

    context "when the user inputs 'q'" do
      let(:user_input) { ['q'] }

      include_examples :before_user_input_messages

      it "thanks the player for playing" do
        expect(human).to receive(:puts).with("Thanks for playing!")

        subject
      end

      it "exits the program" do
        expect(human).to receive(:exit)

        subject
      end
    end

    context "when the user inputs something that is NaN" do
      let(:user_input) { [('a'..'p').to_a.sample, 'q'] }

      include_examples :before_user_input_messages

      it "inform the user that it was a invalid input" do
        expect(human).to receive(:puts).with("Invalid input. Please try again...")

        subject
      end

      it "requests a new input for the user" do
        expect(human).to receive(:gets).twice

        subject
      end
    end

    context "when the user inputs a number that isn't a valid square" do
      let(:user_input) { [double.as_null_object, 'q'] }

      before(:each) do
        allow(board).to receive(:has_square?).with(user_input[0]).and_return(false)
      end

      include_examples :before_user_input_messages

      it "inform the user that the choosed square isn't valid" do
        expect(human).to receive(:puts).with("Invalid square! Please try again...")

        subject
      end

      it "requests a new input for the user" do
        expect(human).to receive(:gets).twice

        subject
      end
    end

    context "when the user inputs a square that is already filled" do
      let(:user_input) { [double.as_null_object, 'q'] }

      before(:each) do
        allow(board).to receive(:square_filled?).with(user_input[0]).and_return(true)
      end

      include_examples :before_user_input_messages

      it "inform the user that the choosed square is already filled" do
        expect(human).to receive(:puts).with("Square already filled. Please choose another one...")

        subject
      end

      it "requests a new input for the user" do
        expect(human).to receive(:gets).twice

        subject
      end
    end

    context "when the user inputs a valid square" do
      let(:user_input) { ['4'] }

      before(:each) do
        allow(board).to receive(:has_square?).with('4').and_return(true)
        allow(board).to receive(:square_filled?).with('4').and_return(false)
      end

      include_examples :before_user_input_messages

      it "returns the input as an integer" do
        expect(board).to receive(:[]=).with(4, marker)

        subject
      end
    end
  end

end
