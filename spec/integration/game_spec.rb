require 'spec_helper'

RSpec.describe "Play" do
  let(:game) { TicTacToe::Game }
  let(:player) { TicTacToe::Player }

  # Replace $stdin and $stdout temporarily to avoid messages on RSpec output
  around(:all) do |example|
    $stdin = $stdout = StringIO.new

    example.run

    $stdin = STDIN
    $stdout = STDOUT
  end

  it "allows users play with each other" do
    allow_any_instance_of(game).to receive(:puts)
    allow_any_instance_of(game).to receive(:print)

    allow_any_instance_of(game).to receive(:gets).and_return(
      "p", # Choose player X to be a person
      "p", # Choose player O to be a person
    )

    allow_any_instance_of(player).to receive(:gets).and_return(
      "4", # Movement 4 for X
      "6", # Movement 6 for O
      "3", # Movement 3 for X
      "7", # Movement 7 for O
      "5"  # Movement 5 for X
    )

    expect_any_instance_of(game).to receive(:print).with("Game over. 'X' marker won!")

    game.run
  end

end
