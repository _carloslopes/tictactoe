# coding: utf-8
# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tictactoe/version'

Gem::Specification.new do |spec|
  spec.name          = 'tictactoe'
  spec.version       = TicTacToe::VERSION
  spec.authors       = ['Carlos Lopes']
  spec.email         = ['carlos.el.lopes@gmail.com']

  spec.summary       = 'Yet another awesome TicTacToe in ruby.'
  spec.description   = 'Yet another awesome TicTacToe in ruby.'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.executables << 'tictactoe'

  spec.require_paths = ['lib']
  spec.files = Dir["{lib}/**/*.rb", "bin/*", "LICENSE", "*.md"]

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
