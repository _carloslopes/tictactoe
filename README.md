# TicTacToe

This is an attempt to implement another version of the famous TicTacToe game.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tictactoe'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tictactoe

## Usage
To run the game, just use install the gem, and run:

    $ tictactoe

## Development

After checking out the repo, run `bundle install` to install dependencies. Then, run `rake spec` to run the tests.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
