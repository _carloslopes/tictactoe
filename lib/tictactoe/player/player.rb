module TicTacToe
  class Player
    MARKERS = ["X", "O"]

    attr_reader :marker

    def initialize(marker)
      @marker = marker
    end

    def play!(board)
      raise NotImplementedError
    end
  end
end
