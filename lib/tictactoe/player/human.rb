module TicTacToe
  class Player::Human < Player

    def play!(board)
      board.print

      square = retrieve_square(board)
      board[square] = @marker
    end

    private
    def retrieve_square(board)
      print "It's your turn '#{@marker}'! Enter [0-8] to play or [q] to quit: "
      input = gets.chomp

      case
      when input == "q"
        puts "Thanks for playing!"
        exit
      when input =~ /\D/
        puts "Invalid input. Please try again..."
        return retrieve_square(board)
      when !board.has_square?(input)
        puts "Invalid square! Please try again..."
        return retrieve_square(board)
      when board.square_filled?(input)
        puts "Square already filled. Please choose another one..."
        return retrieve_square(board)
      end

      input.to_i
    end
  end
end
