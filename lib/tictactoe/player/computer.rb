module TicTacToe
  class Player::Computer < Player
    def initialize(marker, mode: :hard)
      @mode = mode
      super(marker)
    end

    def play!(board)
      square = retrieve_square(board)
      board[square] = @marker
    end

    private
    def retrieve_square(board)
      return 4 unless board.square_filled?(4)
      send("get_#{@mode}_mode_move", board)
    end

    # Easy mode plays randomly
    def get_easy_mode_move(board)
      board.available_squares.sample
    end

    # Medium mode only cares for wins
    def get_medium_mode_move(board)
      available_squares = board.available_squares

      available_squares.each do |square|
        return square if board.game_would_be_over?(square, @marker)
      end

      available_squares.sample
    end

    # Hard mode cares for wins and looses
    def get_hard_mode_move(board)
      available_squares = board.available_squares

      available_squares.each do |square|
        if board.game_would_be_over?(square, @marker) ||
            board.game_would_be_over?(square, opponent_marker)
          return square
        end
      end

      available_squares.sample
    end

    def opponent_marker
      @opponent_marker ||= (MARKERS - [@marker]).first
    end
  end
end
