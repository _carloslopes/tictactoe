require 'forwardable'

module TicTacToe
  class Board
    extend Forwardable
    def_delegators :@squares, :[], :[]=

    WIN_MATCHES = [
      [0, 1, 2], [3, 4, 5], [6, 7, 8],
      [0, 3, 6], [1, 4, 7], [2, 5, 8],
      [0, 4, 8], [2, 4, 6]
    ]

    def initialize
      @squares = Array.new(9)
    end

    def print
      puts <<~BOARD
       #{@squares[0] || 0} | #{@squares[1] || 1} | #{@squares[2] || 2}
      ===x===x===
       #{@squares[3] || 3} | #{@squares[4] || 4} | #{@squares[5] || 5}
      ===x===x===
       #{@squares[6] || 6} | #{@squares[7] || 7} | #{@squares[8] || 8}
      BOARD
    end

    def has_square?(square)
      (0..8).include? Integer(square)
    end

    def square_filled?(square)
      square = Integer(square)
      !@squares[square].nil?
    end

    def has_winner?
      WIN_MATCHES.each do |match|
        squares_values = @squares.values_at(*match).uniq
        return true if squares_values.count == 1 && !squares_values[0].nil?
      end

      false
    end

    def game_is_over?
      has_winner? || all_squares_filled?
    end

    def tie?
      !has_winner? && all_squares_filled?
    end

    def available_squares
      available_squares = []

      @squares.each_with_index do |square, i|
        available_squares << i if square.nil?
      end

      return available_squares
    end

    def game_would_be_over?(square, marker)
      square = Integer(square)
      @squares[square] = marker

      game_is_over?
    ensure
      @squares[square] = nil
    end

    def marker_won?(marker)
      WIN_MATCHES.each do |match|
        squares_values = @squares.values_at(*match).uniq
        return true if squares_values.count == 1 && squares_values[0] == marker
      end

      false
    end

    private
    def all_squares_filled?
      !@squares.include?(nil)
    end
  end
end
