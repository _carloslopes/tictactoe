module TicTacToe
  class Game
    def initialize
      @board = Board.new
    end

    def self.run
      game = new
      game.setup
      game.start
      game.summary
    end

    def setup
      puts "Welcome! Want to play TicTacToe?"

      @player1 = retrieve_player("X")
      @player2 = retrieve_player("O")
    end

    def start
      puts "Allright, let's do it!"

      loop do
        @player1.play!(@board)
        break if @board.game_is_over?

        @player2.play!(@board)
        break if @board.game_is_over?
      end
    end

    def summary
      if @board.tie?
        print "Game tied!"
      elsif @board.marker_won?(@player1.marker)
        print "Game over. '#{@player1.marker}' marker won!"
      else
        print "Game over. '#{@player2.marker}' marker won!"
      end

      puts " Here is the final board:"
      @board.print
    end

    private
    def retrieve_player(marker)
      print "Who will play with the #{marker} marker? Enter [p] for a person, [c] for computer or [q] to quit: "
      input = gets.chomp

      case input
      when "q"
        puts "Thanks for playing!"
        exit
      when "p"
        Player::Human.new(marker)
      when "c"
        retrieve_computer_player(marker)
      else
        puts "Invalid input. Try again..."
        retrieve_player(marker)
      end
    end

    def retrieve_computer_player(marker)
      print "Nice! How smart will be the computer? Enter [n] for noobie, [s] for smater or [c] for Chuck Norris: "
      input = gets.chomp

      case input
      when "n"
        Player::Computer.new(marker, mode: :easy)
      when "s"
        Player::Computer.new(marker, mode: :medium)
      when "c"
        Player::Computer.new(marker, mode: :hard)
      else
        puts "Invalid input. Try again..."
        retrieve_computer_player(marker)
      end
    end
  end
end
